function canoErr = verifyRightCano(A, SA, SB)
%* A(a,i,b)

SA = diag(SA) ;
SB = diag(SB) ;

%* M(a1,m,b2) = sum{b1}_[A(a1,m,b1) * SA(b1,b2)]
M = contractTensors(A, 3, 3, SA, 2, 1) ;
%* M(a1,a2) = sum{m,b2}_[M(a1,m,b2) * A(a2,m,b2)]
M = contractTensors(M, 3, [2, 3], A, 3, [2, 3]) ;
eta = SB(1,1) / M(1, 1) ; 
canoErr = norm(M * eta - SB) ;


