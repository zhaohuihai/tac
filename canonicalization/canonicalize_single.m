function [A, S] = canonicalize_single(parameter, A)
%* A(a,i,b)

[da, di, ~] = size(A) ;

%* A(a,i,b) -> A((a,i), b)
A = reshape(A, [da * di,  da]) ;

%* A((a,i), b) = sum{b1}_[Q((a,i),b1)*R(b1,b)]
[~, R] = qr(A, 0) ;
R = R ./ norm(R) ;
%* A((b,j),c) -> A(b,(j,c))
A = reshape(A, [da, di * da]) ;

%* A(b,(j,c)) = sum{b2}_[L(b,b2)*Q(b2,(j,c))]
% [L, ~] = lq(A) ;
L = lq(A) ;
L = L ./ norm(L) ;
%* A(b,(j,c)) -> A(b,j,c)
A = reshape(A, [da, di, da]) ;

%* RL(b1,b2) = sum{b}_[R(b1,b)*L(b,b2)]
RL =  R * L ;

%* S0(b)
S0 = svd(RL) ;
S0 = S0 ./ sqrt(sum(S0.^2)) ;

converge_cano = 1 ;
tol = parameter.cano.tol ;
maxIt = parameter.cano.maxIt ;

step = 1 ;
while converge_cano > tol && step <= maxIt 
    step = step + 1 ;
    
    %* R(b1,b)
    R = computeRightResidual(A, R) ;
    
    %* L(b,b2)
    L = computeLeftResidual(A, L) ;

    %* RL(b1,b2) = sum{b}_[R(b1,b)*L(b,b2)]
    RL =  R * L ;
    
    [U, S, V] = svd(RL) ;
    S = diag(S) ;
%     coef = sqrt(sum(S.^2)) ;
    coef = sum(S) ;
    S = S ./ coef ;
    
    converge_cano = norm(S - S0) / norm(S) ;

    S0 = S ;
end
% step
% S = S .* coef ;
%* Create Projector
sqrtS = sqrt(S) ;
R2 = L' ;
%* PR(b1,b) = sum{b2}_[R2(b2,b1)*V(b2,b)/sqrtS(b)]
PR = createProjector(R2, V, sqrtS) ;
%* PL(a1,a) = sum{a2}_[R(a2,a1)*U(a2,a) /sqrtS(a)]
PL = createProjector(R, U, sqrtS) ;


%* transform the MPS into its canonical form
%* A(a1,i,b) = sum{b1}_[A(a1,i,b1) * PR(b1,b)]
A = contractTensors(A, 3, 3, PR, 2, 1) ;
%* A(a,i,b) = sum{a1}_[PL(a1,a) * A(a1,i,b)]
A = contractTensors(PL, 2, 1, A, 3, 1) ;


% verifyCano_single(A, S) ;
