function [A, S, truncErr] = truncate_single(A, S, chi)

A = A(1 : chi, :, 1 : chi) ;

truncErr = 1 - sum(S(1 : chi).^2) / sum(S.^2) ;

S = S(1 : chi) ;